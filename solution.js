const got = require("./problemStatement/data-1.js");

countAllPeople(got);
peopleByHouses(got);
everyone(got);
nameWithS(got);
nameWithA(got);
surnameWithS(got);
surnameWithA(got);
peopleNameOfAllHouses(got);

function peopleNameOfAllHouses(got) {
  try {
    let peopleNameOfAllHouses = Object.entries(got).reduce(
      (acc, [, houseNames]) => {
        //used reduce on array(got by entries method)
        houseNames.forEach((houseDetails) => {
          const { name, people } = houseDetails; //destructure houseDetails
          people.forEach((personalDetail) => {
            if (acc.hasOwnProperty(name) === false) {
              //to check if acc has name property or not
              acc[name] = [];
              acc[name].push(personalDetail.name); //if not create empty array and push the person name
            } else {
              acc[name].push(personalDetail.name); //push it to previous tally in array
            }
          });
        });
        return acc;
      },
      {}
    );
    console.log(peopleNameOfAllHouses);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}

function surnameWithA(got) {
  try {
    let allPeopleWithSurnameA = Object.entries(got).reduce(
      (acc, [, houseNames]) => {
        //used reduce on array(got by entries method)
        houseNames.forEach((houseDetails) => {
          const { people } = houseDetails;
          people.forEach((personDetail) => {
            const { name } = personDetail;
            let length = name.split(" ").length; //split the name with separator (" ") and calculated its length
            if (name.split(" ")[length - 1].slice(0, 1) === "A") {
              //to check if surname starts with A or not(used slice)
              acc.push(name); //push into accumulator
            }
          });
        });
        return acc;
      },
      []
    );
    console.log(allPeopleWithSurnameA);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}

function surnameWithS(got) {
  try {
    let allPeopleWithSurnameS = Object.entries(got).reduce(
      (acc, [, houseNames]) => {
        //used reduce on array(got by entries method)
        houseNames.forEach((houseDetails) => {
          const { people } = houseDetails;
          people.forEach((personDetail) => {
            const { name } = personDetail;
            let length = name.split(" ").length; //split the name with separator (" ") and calculated its length
            if (name.split(" ")[length - 1].slice(0, 1) === "S") {
              //to check if surname starts with S or not (used slice).
              acc.push(name); //push into accumulator
            }
          });
        });
        return acc;
      },
      []
    );
    console.log(allPeopleWithSurnameS);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}

function nameWithA(got) {
  try {
    let allPeopleWithA = Object.entries(got).reduce((acc, [, houseNames]) => {
      //used reduce on array(got by entries method)
      houseNames.forEach((houseDetails) => {
        const { people } = houseDetails;
        people.forEach((personDetail) => {
          const { name } = personDetail;
          if (name.includes("a") || name.includes("A")) {
            //to check if name includes a or A
            acc.push(name); //if yes, then push it into accumulator
          }
        });
      });
      return acc;
    }, []);
    console.log(allPeopleWithA);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}

function nameWithS(got) {
  try {
    let allPeoplewithS = [];
    Object.entries(got).forEach(([, houseNames]) => {
      //used reduce on array(got by entries method)
      houseNames.forEach((houseDetail) => {
        const { people } = houseDetail;
        people.forEach((personDetail) => {
          const { name } = personDetail;
          if (name.includes("s") || name.includes("S")) {
            //to check if name includes s or S
            allPeoplewithS.push(name); //if yes, then push it into accumulator
          }
        });
      });
    });
    console.log(allPeoplewithS);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}

function everyone(got) {
  try {
    let allPeople = []; //created an empty object
    Object.entries(got).forEach(([, houseNames]) => {
      houseNames.forEach((houseDetail) => {
        const { people } = houseDetail;
        people.forEach((personDetail) => {
          //iterate over person array
          const { name } = personDetail;
          allPeople.push(name); //push all the names into allPeople array
        });
      });
    });
    console.log(allPeople);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}

function peopleByHouses(got) {
  try {
    let peopleInHouses = {}; //creted an empty object
    Object.entries(got).forEach(([, houseNames]) => {
      // let allPeople = 0;
      houseNames.forEach((houseDetail) => {
        //iterate over houseNames
        const { name, people } = houseDetail;
        peopleInHouses[name] = people.length; //length property is used to check how many people in people arrray
      });
    });
    console.log(peopleInHouses);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}

function countAllPeople(got) {
  try {
    // console.log(got);
    // console.log(Object.entries(got));
    let allPeople = 0;
    Object.entries(got).forEach(([, houseNames]) => {
      // console.log(houseKey);
      // console.log(houseNames);
      // console.log(houseNames.length);
      houseNames.forEach((eacHouseDeatil) => {
        //iterate over houseNames
        const { people } = eacHouseDeatil;
        allPeople += people.length; //Add people length in AllPeople
      });
    });
    console.log(allPeople);
  } catch (error) {
    console.error("An error occurred:", error.message);
  }
}
